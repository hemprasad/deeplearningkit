Language Modeling with Long Short Term Memory Units
============================
Forked from https://github.com/wojzaremba/lstm.

Modifications
--------------
* Runs on CPU and GPU
* Added more comments
* Removed traces of character-level language model
